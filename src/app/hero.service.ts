import { Injectable } from '@angular/core';
import { Hero } from './hero';
import { HEROES } from './mock-heroes';
import { MessageService } from './message.service';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions } from '@angular/http';

@Injectable()
export class HeroService {
  	/** GET heroes from the server */
	getHeroes (): Observable<Hero[]> {
		let options = { headers: new  HttpHeaders({ 'Access-Control-Allow-Origin': '*'})};
		var responseData = this.http.get<Hero[]>(this.heroesUrl, options);
		console.log('response >>>>>:',responseData);
		return responseData;
	}
  	getHero(id: number): Observable<Hero> {
	  // TODO: send the message _after_ fetching the hero
		this.messageService.add(`HeroService: fetched hero id=${id}`);
		return of(HEROES.find(hero => hero.id === id));
	}
  /** Log a HeroService message with the MessageService */
	private log(message: string) {
	  this.messageService.add('HeroService: ' + message);
	}

  private heroesUrl = 'http://localhost:4545/api/test';

  constructor(
  	private http: HttpClient,
  	private messageService: MessageService
  	) { }

}
